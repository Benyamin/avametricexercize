# AvametricExercize
- This is a coding exercize provided by Avametric. 

# How to run the codes?
- Go to any question folder and execute run.sh. This will compile the code, dump log and results to file.

# System requirements:
- OS: Linux (Ubuntu 14.04)
- Compiler: g++ version 11
 
# Author: Benyamin Gholami
# Email: bgholam1@asu.edu

