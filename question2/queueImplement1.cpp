#include "queueInterface.h"

class QueueImplement1 : public QueueInterface {
    int *arr;       // array to store queue elements
    int capacity;   // maximum capacity of the queue
    int front;      // front points to front element in the queue (if any)
    int rear;       // rear points to last element in the queue
    int count;      // current size of the queue
public:
    QueueImplement1(int qsize = 10);
    int pop();
    void push(int value);
    uint64_t size() const noexcept;

    bool isEmpty() const;
    bool isFull() const;
};

QueueImplement1::QueueImplement1(int qsize) {
    arr = new int[qsize];
    capacity = qsize;
    front = 0;
    rear  =-1;
    count = 0;
}

// Remove element from queue
int QueueImplement1::pop() {
    // Check if queue is empty
    if (isEmpty()) {
        std::cout << "Queue is empty!" << std::endl;
        exit(0);
    }
    auto v = arr[front]; 
    front = (front + 1) % capacity;
    count--; 
    return v;
}

// Add element to queue
void QueueImplement1::push(int value) {
    // Check if queue is full
    if (isFull()) {
        std::cout << "Queue is full!" << std::endl;
        exit(0);
    }

    std::cout << "Interting " << value << '\n';
    rear = (rear + 1) % capacity;
    arr[rear] = value;
    count++; 
}

// Return the size of queue
uint64_t QueueImplement1::size() const noexcept {
    return count;
}

// Check if the queue is empty or not
bool QueueImplement1::isEmpty() const {
    return (size() == 0);
}

// Check if the queue is full or not
bool QueueImplement1::isFull() const {
    return (size() == capacity);
}

