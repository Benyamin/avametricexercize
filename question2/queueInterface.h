#include <cstdint>
#include <stdint.h>
class QueueInterface {
public:
/*! Returns and removes the integer at the front of the queue.
Should throw an std::out_of_range exception if the queue is
currently empty
*/
virtual int pop() = 0;
/*! Inserts an integer at the back of the queue
*/
virtual void push(int value) = 0;
/*! Returns the current size of the queue
*/
virtual uint64_t size() const noexcept = 0;
};

