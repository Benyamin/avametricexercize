#!/bin/bash

# Clean previous compile
rm *.o 

# Compile the code and dump compile log to file
g++ -std=c++11  main.cpp -o output.o 2>&1 | tee  compile.log

# Execute the program
./output.o | tee  result.txt
