#include <iostream>
#include <cstdlib>
#include "queueImplement1.cpp"

// main function
int main() {
    int s = 0;
    std::cout << "Enter size of queue:  ";
    std::cin >>  s;
    if (std::cin.fail()) {
        std::cout << "Input has to be an integer!" << std::endl;
        exit(0);
    }
    // create a queue of capacity 5
    QueueImplement1 q(s);

    q.push(1);
    q.push(2);
    q.push(3);
    
    std::cout << "Removing " << q.pop() << std::endl;
    
    q.push(4);

    std::cout << "Queue size is " << q.size() << std::endl;

    q.pop();
    q.pop();
    q.pop();
    
    if (q.isEmpty())
        std::cout << "Queue Is Empty\n";
    else
        std::cout << "Queue Is Not Empty\n";

    return 0;
}

